import React, { Component } from 'react';
import './styles/css/image-gallery.css';
import "../node_modules/react-image-gallery/styles/css/image-gallery.css";
import ImageGallery from 'react-image-gallery';

const PREFIX_URL = 'http://s.qarson.com/cars';
class App extends Component {
    constructor() {
        super();
        this.state = {

        }

        this.images = [
            {
                original: 'http://s.qarson.com/cars/32391/opel-corsa-3p-LF30.qhd.jpg',
                thumbnail: 'http://s.qarson.com/cars/32391/opel-corsa-3p-LF30.qop.jpg',
            },
            {
                original: 'http://s.qarson.com/cars/32390/opel-corsa-3p-LF30.ghd.jpg',
                thumbnail: 'http://s.qarson.com/cars/32390/opel-corsa-3p-LF30.qop.jpg',
            },
        ];
    }

  render() {

    return (
            <ImageGallery
                showPlayButton={false}
                media={'max-width: 700px'}
                items = {this.images}
                additionalClass="app-image-gallery"
            />
    );
  }
}

export default App;
